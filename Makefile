.PHONY: all

PROJ_NAME = supervisor-clone
C_SRC = main.c
CFLAGS = -std=c99 -Wall -Wextra

all: ${C_SRC}
	gcc -o ${PROJ_NAME} ${C_SRC} ${CFLAGS}
