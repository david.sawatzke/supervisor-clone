FROM alpine
WORKDIR /
RUN apk add --no-cache make gcc libc-dev linux-headers
CMD ["sh"]
