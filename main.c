#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#define DEBUG(...) printf(__VA_ARGS__)
pid_t child_pid = -1;

void sig_reap_chld(int signo)
{
	int status, exit_status;
	pid_t killed_pid;
	while ((killed_pid = waitpid(-1, &status, WNOHANG)) > 0) {
		if (WIFEXITED(status)) {
			exit_status = WEXITSTATUS(status);
			DEBUG("A child with PID %d exited with exit status %d.\n", killed_pid, exit_status);
		} else {
			exit_status = 128 + WTERMSIG(status);
			DEBUG("A child with PID %d was terminated by signal %d.\n", killed_pid, exit_status - 128);
		}

		if (killed_pid == child_pid) {
			kill(-1); // send SIGTERM to any remaining processes
			DEBUG("Child exited with status %d. Goodbye.\n", exit_status);
			exit(exit_status);
			//If not all processes are gone, docker will kill'em
		}
	}
}

void sig_userexit(int signo)
{
	exit(0);
}

int main(int argc, char **argv)
{
	if (argc < 1) {
		printf("Too few paramenters\n");
		return 1;
	}
	//Change to root so partitions can be unmounted witjout problems
	chdir("/");
	//Register SIGDHLD signal handler
	signal(SIGCHLD,sig_reap_chld);
	//For debug purposes, so we can handle C-c
	signal(SIGINT,sig_userexit);
	child_pid = fork();
	if (child_pid < 0) {
		printf("Unable to fork. Exiting\n");
		return 1;
	} else if (child_pid == 0) {
		execvp(argv[1],&argv[1]);
	} else {
		while(1);
	}
	return 0;
}
